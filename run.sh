#!/usr/bin/env bash

echo "***************************"
echo "GOLANG"
go build test.go
time ./test hello world

echo "***************************"
echo "NODE"
time node test.js hello world

echo "***************************"
echo "RUBY"
time ruby test.rb hello world

echo "***************************"
echo "PYTHON"
time python test.py hello world
